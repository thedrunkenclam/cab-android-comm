

function controller(value){

  switch (value) {

    case 'start' :

                        //connectionChecker = setInterval(streamConnection,2000);

                        $('#block-div').hide();
                        $('.main-nav-container').hide();
                        $('.footer-container').hide();
                        $('#map-canvas').hide();
                        $('#map-location').hide();

                        controller('check-login');




                        break;

    case 'check-login':

                        try {
                          this.MySession = new Session();
                          if(!this.MySession.hasSession()){
                            controller('show-login-form');
                          }
                          else{
                            this.MyAccount = new Account();
                            this.MyAccount.setEmail(this.MySession.getSessionUsername());
                            this.MyAccount.setPsswrd(this.MySession.getSessionPassword());
                            controller('login-account');
                          }
                        } catch (e) {
                          controller('show-login-form');

                        }

                      break;

    case 'show-login-form':
    //Login Form

                      $('.main-container').load('layout/login-form.html');
                      break;

    case 'set-account':
    //Set Account before login
                      try {
                        this.MyAccount = new Account();
                        var u_name = document.getElementById('login-username').value;
                        var p_w = document.getElementById('login-pw').value;
                        this.MyAccount.setEmail(u_name);
                        this.MyAccount.setPsswrd(p_w);
                        this.MySession.setSessionUsername(u_name);
                        this.MySession.setSessionPassword(p_w);
                        controller('login-account');
                      } catch (e) {
                          Materialize.toast('You are not connected',5000);
                      }
                      break;

    case 'login-account' :
      //Login Users Account
                      if(!isConnected()){
                        appToast('Please Connect to Network..', 3000);
                      }else{
                        var AjaxFunc = new ajaxClass('GET', 'login', 'username=' + this.MyAccount.getAccEmail() + '&&pw=' + this.MyAccount.getPsswrd(), '#');
                        this.MyObj = AjaxFunc.getJSON();
                        if(this.MyObj.login_status == 'success'){
      //Check usertype
      //Populate MyInfo;
      //Save Account to Session
      //this.MyObj.drivers Access Drivers
                        DialogObj = new Object();
                        DialogObj.title = "Undefined";
                        DialogObj.text = "Undefined";
                        DialogObj.sender = "Undefined";
                        DialogObj.action = "Undefined";
                        this.MyCabMap = new CabMap();

                        switch (this.MyObj.user_type_desc) {
                           case 'taxi': //Taxi
                              this.MyTaxi = new Taxi(this.MyObj);

                              controller('show-login-as');
                             break;
                           default: //Commuter or Driver
                              MyInfo = new UserInfo(this.MyObj);
                              //set dashboard-view to main;
                              dashboardView = 'main';
                              appToast('PLEASE WAIT', 4000);
                              controller('load-dashboard');
                            break;
                         }
                        }
                        else{
    //Login Fail
                          this.MySession.clearSession();//Uncomment later
                          appToast('Incorrect username or password', 4000);
                          controller('show-login-form');
    //TEST
                        }
                      }
                      break;


    case 'show-login-as' :
                      route($('.main-container'), 'layout/taxi-driver-login.html');
                      break;

    case 'forgot-pw' :

    break;

    case 'show-registration-form':
      //Show Registration form
      //route($('.main-container'), 'layout/wait.html');
                      route($('.main-container'), 'layout/register-form.html');
                      break;


    case 'check-registration-input':
                      if( document.getElementById('reg_fname').value.length <= 0 ||
                          document.getElementById('reg_mname').value.length <=0 ||
                          document.getElementById('reg_lname').value.length <=0 ||
                          document.getElementById('reg_email').value.length <=0 ||
                          document.getElementById('reg_pw').value.length <=0 ||
                          document.getElementById('reg_bdate').value.length <=0
                          ){
                              appToast('Please complete the form', 4000);
                          }else {
                            if(document.getElementById('commuter').checked == false && document.getElementById('driver').checked == false){
                                appToast('Please choose user type', 4000);
                            }else{
                              //check email
                              var tempEmail = document.getElementById('reg_email').value;
                              var varTempEmailLen = tempEmail.split('@');
                              if(varTempEmailLen.length == 2){
                                var tempEmailExt = varTempEmailLen[1].split('.');
                                if(tempEmailExt.length == 2){
                                  if(tempEmailExt[1] == 'com'){
                                    //valid
                                    if(document.getElementById('agree').checked == false){
                                      appToast('Agree to terms to continue..', 3000);
                                    }else {
                                      controller('register-user');
                                    }
                                  }else{appToast('Please input valid email', 3000);}
                                }else{appToast('Please input valid email', 3000);}
                              }else{appToast('Please input valid email', 3000);}
                            }
                      }
                      break;


    case 'register-user':
                      var new_fname = document.getElementById('reg_fname').value;
                      var new_mname = document.getElementById('reg_mname').value;
                      var new_lname = document.getElementById('reg_lname').value;
                      var new_unhashemail = document.getElementById('reg_email').value;
                      var new_unhashpw = document.getElementById('reg_pw').value;
                      //Hash email and pw
                      var hashEmailAjax = new ajaxClass('GET', 'hash', 'value=' + new_unhashemail , '#');
                      var new_email = hashEmailAjax.getJSON().result;
                      var hashPwAjax = new ajaxClass('GET', 'hash', 'value=' + new_unhashpw , '#');
                      var new_pw = hashPwAjax.getJSON().result;
                      var new_bdate = getDate(document.getElementById('reg_bdate').value);
                      var new_contact = document.getElementById('reg_contact').value;
                      var new_user_type;
                      if(document.getElementById('commuter').checked == true){
                        new_user_type = 0; //Com]muter usertype
                      }else{
                        new_user_type = 1; //Driver user Type
                      }
                      //Call Ajax Registration class
                      var ajaxFunc = new ajaxClass('GET', 'register', 'user_type=' + new_user_type +
                                    '&fname=' + new_fname + '&mname=' + new_mname + '&lname=' + new_lname +
                                    '&email=' + new_email + '&bdate=' + new_bdate + '&contact=' + new_contact +
                                    '&pw=' + new_pw, '#');
                      // appToast(JSON.stringify(ajaxFunc.getJSON()));
                      if(ajaxFunc.getJSON().status == 1){
                        //SUCCESS
                        //Create new users account
                        appToast('Success Registration!', 1000);
                        MyAccount = new Account();
                        MyAccount.setEmail(new_unhashemail);
                        MyAccount.setPsswrd(new_unhashpw);
                        controller('login-account');
                      }else{
                        //FAIL
                        appToast('Oops! email is already used', 4000);
                      }
                      break;

    case 'load-dashboard':
      //Load user dashboard
      //First NavBar;
                      $('.main-nav-container').css('height', '10%');
                      $('.main-container').css('height', '80%');
                      $('.footer-container').css('height', '10%');
                      route($('.main-container'), 'layout/wait.html');
                      controller('load-navbar');
                      controller('load-body');
                      controller('load-footer');
                      // appToast('lat=' + this.MyCabMap.getLat() + 'lang' + this.MyCabMap.getLang());
                      break;

    case 'load-navbar':
      //Load navbar

                      try {

                        $('.main-nav-container').show();
                        var user_type = this.MyObj.user_type_desc;
                        var elem = 'navbar';
                        var AjaxFunc = new ajaxClass('GET', 'dashboard', 'user_type=' + user_type +'&&view='+ dashboardView +'&&element=' + elem,  $('.main-nav-container'));
                        AjaxFunc.loadedToElement();
                      } catch (e) {
                        $('.main-container').load('layout/err.html');
                      }
                      break;

    case 'load-body':
                      try {

                        $('.main-container').show();
                        var user_type = this.MyObj.user_type_desc;
                        var elem = 'body';
                        var AjaxFunc = new ajaxClass('GET', 'dashboard', 'user_type=' + user_type +'&&view='+ dashboardView + '&&element=' + elem,  $('.main-container'));
                        AjaxFunc.loadedToElement();
                      } catch (e) {
                        $('.main-container').load('layout/err.html');
                      }



                      break;

    case 'load-footer':
      //Load user dashboard

                        $('.footer-container').show();
                        var user_type = this.MyObj.user_type_desc;
                        //var user_id = this.MyInfo.getUserId();
                        var elem = 'footer';
                        var AjaxFunc = new ajaxClass('GET', 'dashboard', 'user_type=' + user_type +'&&view='+ dashboardView + '&&element=' + elem,  $('.footer-container'));
                        AjaxFunc.loadedToElement();
                      break;

    case 'load-account-details':
                      document.getElementById('my-account-fullname').innerHTML = this.MyInfo.getFullname();
                      document.getElementById('my-account-contact').innerHTML = this.MyInfo.getContact();
                      document.getElementById('my-account-bdate').innerHTML = this.MyInfo.getBdate();
                      document.getElementById('my-account-username').value = this.MyAccount.getAccEmail();
                      var pw='';
                      for(var i=0; i<this.MyAccount.getPsswrd().length; i++){
                        pw = pw + '*';
                      }
                      document.getElementById('my-account-pw').value = pw;

                      var img = "<img class='responsive-img' src='" + this.MyInfo.getPhoto() + "'></img>";
                      $('#account-img-holder').append(img);
    break;



    case 'account':
                      dashboardView = 'account';
                      // controller('load-navbar');
                      controller('load-body');
                      controller('load-footer');
    break;


    case 'load-update-account-form' :
                      dashboardView = 'update-account-form';
                      controller('load-body');
    break;


    case 'check-update-my-account' :

                        this.tempUserInfo = new Object();
                        var err = false;
                        if(update_fname){if(isValidInput(document.getElementById('update_fname'))){this.tempUserInfo.fname = document.getElementById('update_fname').value;}else{err = true;}}else{this.tempUserInfo.fname = this.MyInfo.getFname();}
                        if(update_mname){if(isValidInput(document.getElementById('update_mname'))){this.tempUserInfo.mname = document.getElementById('update_mname').value;}else{err = true;}}else{this.tempUserInfo.mname = this.MyInfo.getMname();}
                        if(update_lname){if(isValidInput(document.getElementById('update_lname'))){this.tempUserInfo.lname = document.getElementById('update_lname').value;}else{err = true;}}else{this.tempUserInfo.lname = this.MyInfo.getLname();}
                        if(update_email){if(isValidInput(document.getElementById('update_email'))){this.tempUserInfo.email = document.getElementById('update_email').value;}else{err = true;}}else{this.tempUserInfo.email = this.MyAccount.getAccEmail();}
                        if(update_contact){if(isValidInput(document.getElementById('update_contact'))){this.tempUserInfo.contact = document.getElementById('update_contact').value;}else{err = true;}}else{this.tempUserInfo.contact = this.MyInfo.getContact();}
                        if(update_bdate){if(isValidInput(document.getElementById('update_bdate'))){this.tempUserInfo.bdate = document.getElementById('update_bdate').value;}else{err = true;}}else{this.tempUserInfo.bdate = this.MyInfo.getBdate();}
                        if(update_pw){if(isValidInput(document.getElementById('update_pw'))){this.tempUserInfo.pw = document.getElementById('update_pw').value;}else{err = true;}}else{this.tempUserInfo.pw = this.MyAccount.getPsswrd();}
                        if(!err){
                          appToast('Updating Account', 2000);
                          controller('update-my-account');
                        }else{
                          //Fail
                          appToast('Please complete the form..', 4000);
                        }

    break;

    case 'update-my-account' :
    //Check User Input for Update account
                        var ajaxFunc = new ajaxClass('GET', 'update-account', 'id=' + this.MyInfo.getUserId() + '&user_type=' + this.MyObj.user_type_id +
                                                   '&fname=' + this.tempUserInfo.fname + '&mname=' + this.tempUserInfo.mname + '&lname=' + this.tempUserInfo.lname +
                                                   '&bdate=' + this.tempUserInfo.bdate + '&contact=' + this.tempUserInfo.contact + '&email=' + this.tempUserInfo.email +
                                                   '&pw=' + this.tempUserInfo.pw, '#');
                          if(ajaxFunc.getJSON().status == 1){
                          //Success Update
                          this.MySession.setSessionUsername(this.tempUserInfo.email);
                          this.MySession.setSessionPassword(this.tempUserInfo.pw);
                          this.MyAccount.setEmail(this.tempUserInfo.email);
                          this.MyAccount.setPsswrd(this.tempUserInfo.pw);
                          this.MyInfo.setFname(this.tempUserInfo.fname);
                          this.MyInfo.setMname(this.tempUserInfo.mname);
                          this.MyInfo.setLname(this.tempUserInfo.lname);
                          this.MyInfo.setBdate(this.tempUserInfo.bdate);
                          this.MyInfo.setContact(this.tempUserInfo.contact);
                          controller('account');
                        }else{
                          //Fail
                          appToast('Error Updating', 4000);
                        }

    break;

    case 'upload':
    //Photo upload
                        appToast('Opening.', 4000);
                        getImage(this.MyInfo.getUserId());

    break;

    case 'logout-account' :
                        route($('.main-container'), 'layout/wait.html');

                        var ajaxFunc = new ajaxClass('GET', 'logout', 'id=' + this.MyInfo.getAccountNo() , '#');
                        if(ajaxFunc.getJSON().status == 1){
                          if(this.MyObj.user_type_desc=='taxi'){
                            //Update taxi_driver_logs

                            var tdLogs = new ajaxClass('GET', 'update-taxi-driver-logs', 'id=' + this.MyTaxiDriverLogs.getTdId(), '#');
                            if(tdLogs.getJSON().status == 'success'){
                              controller('start');
                            }else{
                              appToast('Error: Please Try Again!', 3000);
                            }
                          }else{
                            this.MySession.clearSession();
                            appToast('Logout', 4000);
                            controller('start');
                          }
                        }else{
                          appToast('Logging out failed', 4000);
                        }
    break;


    case 'settings' :

                        route($('#main-container'), 'layout/settings.html');
    break;

    case 'go-online' :
    //function for Taxi
                        var ajaxFunc = new ajaxClass('GET', 'is-online', 'acc_no=' + this.MyTaxi.getAccNo() , '#');
                        var onStatus = ajaxFunc.getJSON().status;
                        if(onStatus == 1){
                          var goOffline = new ajaxClass('GET', 'go-offline', 'acc_no=' + this.MyTaxi.getAccNo(), '#');
                          if(goOffline.getJSON().status == 'success'){
                            //Send to Server Current Location
                              clearInterval(waitTripRequest);
                              $('#go-online-btn').removeClass('offline');
                              $('#go-online-btn').addClass('online');
                              document.getElementById('go-online-btn').innerHTML = 'Go Online';
                            //Success offline account
                            //stop waiting function;
                          }else{
                            //Error offline account
                            appToast('Please try again later', 1000);
                          }
                        }else if (onStatus==0) {
                          var goOnline = new ajaxClass('GET', 'go-online', 'acc_no=' + this.MyTaxi.getAccNo(), '#');
                          //Update Taxi Location

                          if(goOnline.getJSON().status == 'success'){
                            //Success online account
                            var myCurrentLoc = new ajaxClass('GET', 'set-my-location', 'id=' + MyTaxi.getTaxiId() + '&lat=' + MyLocation.getLat() + '&lang=' + MyLocation.getLang(), '#');
                            if(myCurrentLoc.getJSON().status =='success'){

                              // var setMyLocation = new ajaxClass('GET', 'set-my-location', 'id=' + MyTaxi.getTaxiId() + '&lat=' + MyLocation.getLat() + '&lang=' + MyLocation.getLang() , '#');
                              // var res = setMyLocation.getJSON();

                              $('#go-online-btn').removeClass('online');
                              $('#go-online-btn').addClass('offline');
                              document.getElementById('go-online-btn').innerHTML = 'Go Offline';
                              waitTripRequest = setInterval(getRequest, 2000);
                            } else {
                               appToast('Error Sending Location', 3000);
                            }
                            //wait for request;
                          }else{
                            //Error online account
                            appToast('Please try again later', 1000);
                          }
                        }else{
                          appToast('Please try again later', 2000);
                        }
    break;

    case 'check-pre-trip':
                        try {
                          var destination = document.getElementById('autocomplete-input').value;
                          var isValid = false;
                          if(destination.length > 0 || destination == null){
                              var destinationList = getDestinations('list');
                              for(var i=0; i<destinationList.data.length; i++){
                                if(destination == destinationList.data[i]){
                                    isValid = true;
                                }
                              }
                              //check validity
                              if(!isValid){
                                //Continue
                                appToast('Cannot find Destination',3000);
                              }else{
                                this.MyTrip = new Trip();
                                this.MyTrip.setDestination(destination);
                                controller('show-pre-trip');
                              }
                          }else{

                            appToast('No Destination', 3000);

                          }
                        } catch (e) {
                          appToast(e);
                        }

    break;



    case 'show-pre-trip' :

                        dashboardView = 'pre-trip';
                        var preDetails = new ajaxClass('GET', 'dashboard', 'user_type=' + this.MyObj.user_type_desc + '&view=' + dashboardView + '&element=body', $('#map-location'));
                        preDetails.loadedToElement();
                        controller('load-footer');

    break;


    case 'back-to-main-dashboard':

                        route($('.main-container'), 'layout/wait.html');
                        dashboardView = 'main';
                        controller('load-dashboard');
    break;

    case 'send-trip-request' :
                        var setLoc = new ajaxClass('GET', 'set-my-location', 'id=' + this.MyInfo.getAccountNo() + '&lat=' +this.MyLocation.getLat() + '&lang=' + this.MyLocation.getLang() , '#');
                        var setLocJSON = setLoc.getJSON();
                        //var dist = new ajaxClass('GET', 'get-taxi-list', 'coor=13.632039,123.186412', '#');
                        //Set Location
                        //Success Insert Location
                        var dist = new ajaxClass('GET', 'get-taxi-list', 'coor=' + this.MyLocation.getLat() + ',' + this.MyLocation.getLang(), '#');
                        var list = dist.getJSON();

                        if(list.length > 0){
                        //Get taxi id and details from server and store to tempArr;
                          var taxiList = storeToArray(list);
                          //Get the nearest taxi and sort the list;
                          taxiList = sortArray(taxiList);
                          //temparr is now sorted from nearest to farthest
                          //create new trip
                          var newTripJSON = newTrip();
                          if(newTripJSON.status == 'success'){
                            this.MyTrip.setTripByObj(newTripJSON);
                            //Send request
                            setTaxiIndex(0);
                            setTaxiListArray(taxiList);
                            sendMyTripRequest();
                            showBlockMessage('Please wait',3000);
                          } else {
                            appToast('Error Add Trip',3000);
                          }
                        }else{
                          appToast('No Available Taxi', 3000);
                        }

    break;



  }//End Switch
}//End Function

//Files to change IPADDRESS / URL
//Controller.js, cab-map.js, index.html, user.js, ajax_functions.js
function streamConnection(){
  try {
    var connection = JSON.parse($.ajax({
        type: 'GET',
        url: 'http://cab-api.6te.net/api/controller.php?type=connection',
        dataType: 'json',
        global: false,
        async: false,
        success: function (data) {
            return data;
        }
    }).responseText);
    if(connection.status==1){
       if(myConnectionToServer){
       }else{
         myConnectionToServer = true;
         //Materialize.toast('You are now connected!');
         $('#block-div').hide();
       }
    }else{
      if(!myConnectionToServer){
      }else{
        myConnectionToServer = false;
        Materialize.toast('You are Disconnected');
        $('#block-div').show();
      }
    }
  } catch (e) {
    if(!myConnectionToServer){
    }else{
      myConnectionToServer = false;
      Materialize.toast('You are Disconnected');
      $('#block-div').show();
    }
  }
}
