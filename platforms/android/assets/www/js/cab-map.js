

var CabMap = function(){
  this.lat;
  this.lang;
  this.type;
  this.myLatlng;
  this.myMapOptions
  this.myMap;
  this.watchID;
};

//===Commuter
CabMap.prototype.loadTripMap = function(){
  this.watchID = navigator.geolocation.getCurrentPosition(this.tripSuccess, this.onError);
};
CabMap.prototype.loadTaxiTripMap = function () {
  this.watchID = navigator.geolocation.getCurrentPosition(this.successTaxiTrip,this.onError);
};
CabMap.prototype.loadTaxiOnTripMap = function () {
  this.watchID = navigator.geolocation.getCurrentPosition(this.successTaxiOnTrip,this.onError);
};

CabMap.prototype.tripSuccess = function(position){


  $('#map-location').html("");
  var lat = position.coords.latitude;
  var lang= position.coords.longitude;

  var setMyLocation = new ajaxClass('GET', 'set-my-location', 'id=' + MyInfo.getAccountNo() + '&lat=' + lat + '&lang=' + lang, '#');
  var res = setMyLocation.getJSON();

  this.myLatlng = new google.maps.LatLng(lat, lang);
  this.myMapOptions = {zoom: 14,center: this.myLatlng}
  this.myMap = new google.maps.Map((document.getElementById('map-canvas')), this.myMapOptions);
  var myMarker = new google.maps.Marker({
    position: this.myLatlng,
    map: this.myMap,
    icon: "http://cab-api.6te.net/api/mobile/img/location.png"
  });

  var myTaxiCoor = new ajaxClass('GET', 'get-taxi-location', 'id=' + MyTrip.getTaxi(), '#');
  var myTaxiCoorJSON = myTaxiCoor.getJSON();
  if(myTaxiCoorJSON.status == 'success'){
    var myTaxiLatLng = new google.maps.LatLng(myTaxiCoorJSON.lat, myTaxiCoorJSON.lang);
    var myTaxiMarker = new google.maps.Marker({
      position: myTaxiLatLng,
      map: this.myMap,
      icon: "http://cab-api.6te.net/api/mobile/img/taxi.png"
    });
  } else {
    appToast('Cant Load Taxi Location', 3000);
  }

  route($('#map-location'), 'layout/trip-duration-details.html');

 };
 //===END----



 CabMap.prototype.successTaxiOnTrip = function (position) {
   $('#map-location').hide();
   var lat = position.coords.latitude;
   var lang= position.coords.longitude;

   var setMyLocation = new ajaxClass('GET', 'set-my-location', 'id=' + MyTaxi.getTaxiId() + '&lat=' + lat + '&lang=' + lang, '#');
   var res = setMyLocation.getJSON();

   this.myLatlng = new google.maps.LatLng(lat, lang);
   this.myMapOptions = {zoom: 16,center: this.myLatlng}
   this.myMap = new google.maps.Map((document.getElementById('map-canvas')), this.myMapOptions);
   var myMarker = new google.maps.Marker({
     position: this.myLatlng,
     map: this.myMap,
     icon: "http://cab-api.6te.net/api/mobile/img/taxi.png"
   });

   //001
   var myDestinationCoor = new ajaxClass('GET', 'get-destination-coordinates', 'name=' + MyTrip.getDestination(),'#');
   var myDestinationCoorJSON = myDestinationCoor.getJSON();
   if(myDestinationCoorJSON.status == 'success'){
     var myDestinationLatLng = new google.maps.LatLng(myDestinationCoorJSON.lat, myDestinationCoorJSON.lang);
     var myDestinationMarker = new google.maps.Marker({
       position: myDestinationLatLng,
       map: this.myMap,
       icon: "http://cab-api.6te.net/api/mobile/img/flag.png"
     });

   } else {
     appToast('Cant Load Destination Location', 3000);
   }

   //set my Location

 };

 CabMap.prototype.successTaxiTrip = function (position) {
   $('#map-location').hide();
   var lat = position.coords.latitude;
   var lang= position.coords.longitude;

   var setMyLocation = new ajaxClass('GET', 'set-my-location', 'id=' + MyTaxi.getTaxiId() + '&lat=' + lat + '&lang=' + lang, '#');
   var res = setMyLocation.getJSON();


   this.myLatlng = new google.maps.LatLng(lat, lang);
   this.myMapOptions = {zoom: 16,center: this.myLatlng}
   this.myMap = new google.maps.Map((document.getElementById('map-canvas')), this.myMapOptions);
   var myMarker = new google.maps.Marker({
     position: this.myLatlng,
     map: this.myMap,
     icon: "http://cab-api.6te.net/api/mobile/img/taxi.png"
   });

   var myTaxiCoor = new ajaxClass('GET', 'get-user-location', 'id=' + MyTrip.getPassenger() + '&desc=commuter', '#');
   var myTaxiCoorJSON = myTaxiCoor.getJSON();
   if(myTaxiCoorJSON.status == 'success'){
     var myTaxiLatLng = new google.maps.LatLng(myTaxiCoorJSON.lat, myTaxiCoorJSON.lang);
     var myTaxiMarker = new google.maps.Marker({
       position: myTaxiLatLng,
       map: this.myMap,
       icon: "http://cab-api.6te.net/api/mobile/img/commuter.png"
     });

   } else {
     appToast('Cant Load Passenger Location', 3000);
   }
 };

 CabMap.prototype.getCurrentCoordinates = function () {
   this.watchID = navigator.geolocation.getCurrentPosition(this.successCoor,this.onError);
 };

 CabMap.prototype.successCoor = function (position) {
   MyLocation.setLat(position.coords.latitude);
   MyLocation.setLang(position.coords.longitude);
 };
CabMap.prototype.loadMyLocation = function () {
  this.watchID = navigator.geolocation.getCurrentPosition(this.onSuccess,this.onError);
};

CabMap.prototype.onSuccess = function (position) {
  try {
    var lat = position.coords.latitude;
    var lang= position.coords.longitude;
    MyLocation = new UserLoc(lat, lang);
    var tempMarker;
    var setMyLocation;
    if(MyObj.user_type_desc == 'taxi'){
      setMyLocation = new ajaxClass('GET', 'set-my-location', 'id=' + MyTaxi.getTaxiId() + '&lat=' + lat + '&lang=' + lang, '#');
    }else {
      setMyLocation = new ajaxClass('GET', 'set-my-location', 'id=' + MyInfo.getAccountNo() + '&lat=' + lat + '&lang=' + lang, '#');
    }
    var res = setMyLocation.getJSON();
    //route($('#map-location'), 'layout/map.html');
    $('#map-location').show();
    var mapF = new ajaxClass('GET', 'get-location-name', 'lat=' + MyLocation.getLat() + '&lang=' + MyLocation.getLang(), $('#my-origin-location'));
    MyLocation.setAddress(mapF.getJSON().address);
    document.getElementById('my-origin-location').innerHTML = MyLocation.getAddress();



    this.myLatlng = new google.maps.LatLng(lat, lang);
    this.myMapOptions = {zoom: 13,center: this.myLatlng}
    this.myMap = new google.maps.Map((document.getElementById('map-canvas')), this.myMapOptions);
    var myMarker = new google.maps.Marker({
      position: this.myLatlng,
      map: this.myMap,
      icon: "http://cab-api.6te.net/api/mobile/img/" + MyObj.user_type_desc +".png"
    });
  } catch (e) {
      appToast(e);
  }


};

CabMap.prototype.onError = function (error) {
    $('#map-canvas').load('layout/error-map.html');

};

CabMap.prototype.addMarker = function (lat, lang, type) {

  this.myLatlng = new google.maps.LatLng(MyLocation.getLat(),MyLocation.getLang());
  this.myMapOptions = {zoom: 13,center: this.myLatlng}
  this.myMap = new google.maps.Map((document.getElementById('map-canvas')), this.myMapOptions);
  var myMarker = new google.maps.Marker({
    position: this.myLatlng,
    map: this.myMap,
    icon: "http://cab-api.6te.net/api/mobile/img/location.png"
  });

    var newLatLang = new google.maps.LatLng(lat, lang);
    var newMarker = new google.maps.Marker({
      position: newLatLang,
      map: this.myMap,
      icon: "http://cab-api.6te.net/api/mobile/img/" + type + ".png"
    });
};

CabMap.prototype.stopWatch = function () {
  var geoLoc = navigator.geolocation;
  geoLoc.clearWatch(this.watchID);
};
